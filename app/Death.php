<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;
use Illuminate\Database\Eloquent\SoftDeletes;

class Death extends Model
{
    use LogsActivity, SoftDeletes;

    protected static $logName = 'deaths';

    protected $hidden = ['created_at', 'updated_at'];

    public function human()
    {
        return $this->belongsTo('App\Human', 'cnp', 'cnp')->withTrashed();
    }
}

<?php

namespace App;

use App\Events\NotificationStored;

use Illuminate\Database\Eloquent\Model;

class Notification extends Model
{
    protected $dispatchesEvents = [
        'created' => NotificationStored::class,
    ];
}

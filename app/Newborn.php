<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;
use Illuminate\Database\Eloquent\SoftDeletes;

class Newborn extends Model
{
    use LogsActivity, SoftDeletes;

    protected static $logName = 'newborns';

    public function human()
    {
        return $this->belongsTo('App\Human', 'cnp', 'cnp')->withTrashed();
    }
}

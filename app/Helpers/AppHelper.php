<?php
namespace App\Helpers;

class AppHelper
{
    public static function refresh($request)
    {   
        $token = explode(" ", $request->headers->get('Authorization'));
        $status = $request->status;
        
        return AppHelper::respondWithToken($token[1], $token[0], $status);
    }

    public static function respondWithToken($token, $token_type, $status) {
        $user = auth()->user();
        return [
            'status' => $status,
            'access_token' => $token,
            'token_type' => $token_type,
            'expires_in' => auth()->factory()->getTTL() . ' min',
            'user' => [
                'first_name' => $user->human->first_name,
                'last_name' => $user->human->last_name,
                'email' => $user->email,
                'role' => $user->role->name,
                'access_level' => $user->role->access_level,
                'avatar' => $user->avatar
            ],
        ];
    }

    public static function respondWithMessage($status, $message, $code = 200)
    {
        return response()->json([
            'status' => $status,
            'message' => $message,
        ], $code);
    }

    public static function shouldRestoreEntity($request, $entity)
    {
        $model = '\\App\\' . $entity;
        $trashedEntities = $model::onlyTrashed()->get();

        foreach ($trashedEntities as $entity) {
            if ($request->cnp == $entity->cnp) {
                $entity->restore();
                return $entity->id;
            }
        }

        return -1;
    }

    public static function deleteEntity($request, $id, $entity)
    {
        $model = '\\App\\' . $entity;
        $entityInstance = $model::find($id);

        if (!$entityInstance) {
            return [
                'status' => 'failed',
                'message' => $entity . ' doesn\'t exists or is already deleted.',
                'auth' => \AppHelper::refresh($request)
            ];
        } else {
            if ($entityInstance->human) {
                $entityInstance->human->delete();
            }
            $entityInstance->delete();
        }
        return [
            'status' => 'success',
            'message' => $entity . ' deleted.',
            'auth' => \AppHelper::refresh($request)
        ];
    }
}

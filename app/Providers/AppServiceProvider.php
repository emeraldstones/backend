<?php

namespace App\Providers;

use App\Patient;
use App\Human;
use App\Notification;

use App\Observers\PatientObserver;
use App\Observers\HumanObserver;
use App\Observers\NotificationObserver;

use Illuminate\Support\ServiceProvider;

use Laravel\Horizon\Horizon; 

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Horizon::auth(function ($request) {
            return true;
        });

        Patient::observe(PatientObserver::class);
        Human::observe(HumanObserver::class);
        Notification::observe(NotificationObserver::class);
    }
}

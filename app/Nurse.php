<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;
use Illuminate\Database\Eloquent\SoftDeletes;

class Nurse extends Model
{
    use LogsActivity, SoftDeletes;

    protected static $logName = 'nurses';

    public function human()
    {
        return $this->belongsTo('App\Human', 'cnp', 'cnp')->withTrashed();
    }

    public function user()
    {
        return $this->hasOne('App\User', 'cnp', 'cnp');
    }
}

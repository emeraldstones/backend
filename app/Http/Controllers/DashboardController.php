<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Controllers\HumanController;
use App\Http\Controllers\PatientController;
use App\Http\Controllers\DoctorController;
use App\Http\Controllers\NurseController;

use App\Http\Requests\HumanFormRequest;
use App\Http\Requests\PatientFormRequest;
use App\Http\Requests\DoctorFormRequest;
use App\Http\Requests\NurseFormRequest;
use App\Human;

class DashboardController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth.refresh', ['except' => []]);

        $this->middleware('isAdmin:api', ['except' => []]);

        $this->middleware('isAdminOrSelf:api', ['except' => ['storePatient', 'storeDoctor', 'storeNurse']]);
    }

    public function fetch(Request $request)
    {
        $humans = Human::all();
        $doctors = [];

        foreach ($humans as $human) {
            array_push($doctors, $human->doctor);
        }

        return response()->json([
            'auth' => \AppHelper::refresh($request),
            'doctors' => $humans
        ], 200);
    }
}

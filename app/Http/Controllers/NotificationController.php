<?php

namespace App\Http\Controllers;

use App\Notification;
use Illuminate\Http\Request;

class NotificationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $sameNotification = Notification::orderBy('created_at', 'desc')
            ->where('message', $request->message)
            ->get();

        if (count($sameNotification) > 0) {
            $now = new \Carbon\Carbon();
            $past = new \Carbon\Carbon($sameNotification[0]->created_at);
            if ($now->diffInSeconds($past) < 10) {
                return response()->json([
                    'status' => 'Notification already stored'
                ]);
            }
        }

        $notification = new Notification();
        $notification->message = $request->message;
        $notification->action = $request->action;
        $notification->color = $request->color;
        $notification->save();

        return response()->json([ 
            'notification' => $notification
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Notification  $notification
     * @return \Illuminate\Http\Response
     */
    public function show(Notification $notification)
    {
        $notifications = Notification::orderBy('created_at', 'desc')->get();
        
        if (auth()->user()->last_notification_read) {
            $unreaded_notifications = [];
            $last_notification_read = new \Carbon\Carbon(auth()->user()->last_notification_read);

            foreach($notifications as $not) {
                $notification_created = new \Carbon\Carbon($not->created_at);

                if ($last_notification_read->lt($notification_created)) {
                    array_push($unreaded_notifications, $not);
                }
            }

            return response()->json([
                'notifications' => $unreaded_notifications
            ]);
        }
        return response()->json([
            'notifications' => $notifications
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Notification  $notification
     * @return \Illuminate\Http\Response
     */
    public function edit(Notification $notification)
    {
    
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Notification  $notification
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Notification $notification)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Notification  $notification
     * @return \Illuminate\Http\Response
     */
    public function destroy(Notification $notification)
    {
        //
    }
}

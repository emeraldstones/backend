<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\UserUpdateRequest;

use Illuminate\Support\Facades\Auth;

use App\Http\Controllers\HumanController;

use App\User;

class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('jwt.auth:api', ['except' => []]);

        $this->middleware('isAdmin:api', ['except' => ['show', 'update', 'delete']]);

        $this->middleware('isAdminOrSelf:api', ['except' => []]);
    }

    public static function update(Request $request, $id)
    {
        //  Get User Instance
        $user = User::find($id);
        //  Get Role Controller Instance
        $roleModel = 'App\\Http\\Controllers\\' . ucfirst($user->role->name) . 'Controller';
        //  Human update
        $humanUpdate = HumanController::update($request, $user->human->id);
        if ($humanUpdate['status'] == 'failed') {
            return $humanUpdate;
        }
        //  Role Update
        if ($user->role->id > 1) {
            $roleModelUpdate = $roleModel::update($request, $user->entity()->id);
            if ($roleModelUpdate['status'] == 'failed') {
                return $roleModelUpdate;
            }
        }
        //  Update user
        $credentialsChanged = false;
        $user->cnp = $request->cnp;
        $user->avatar = $request->avatar ? $request->avatar : 'user.png';
        //  Update password if exists
        if ($request->password) {
            $credentialsChanged = true;
            $user->password = bcrypt($request->password);
        }
        //  Update email if exists
        if ($request->email && $request->email != $user->email) {
            $credentialsChanged = true;
            $user->email = $request->email;
        }
        $user->save();

        if ($credentialsChanged && $user->id == auth()->user()->id) {
            auth()->logout();
            $token = auth()->login($user);
            $request->headers->set('Authorization', 'Bearer ' . $token);
            $request->status = 'changed';
        }
        
        //  Return response
        return [
            'status' => 'success',
            'message' => 'User updated.',
            'auth' => \AppHelper::refresh($request)
        ];
    }

    public function delete($id)
    {
        $user = User::find($id);
        if ($user) {
            $user->delete();

            return response()->json([
                'status' => 'success',
                'message' => 'User deleted.'
            ]);
        }

        return response()->json([
            'status' => 'failed',
            'message' => 'User not found.'
        ]);
    }

    public function index()
    {
        $users = User::all();

        return response()->json([
            'status' => 'success',
            'users' => $users->toArray(),
        ], 200);
    }

    public function show(Request $request, $id)
    {
        $user = User::find($id);

        return response()->json([
            'status' => 'success',
            'user' => $user->toArray(),
        ], 200);
    }

    public function update_avatar(Request $request) {
        $request->validate([
            'avatar' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);

        $user = Auth::user();
    }

    public static function update_role(User $user)
	{
		if ($user->isDoctor()) {
			$role_id = 2;
		} else if ($user->isNurse()) {
			$role_id = 3;
		} else if ($user->isPatient()) {
			$role_id = 4;
		} else {
			$role_id = 0;
        }
        
        return $role_id;
	}
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Appointment;
use Carbon\Carbon;

class AppointmentController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth.refresh', ['except' => []]);

        $this->middleware('isAdmin:api', ['except' => ['show']]);

        $this->middleware('isAdminOrSelf:api', ['except' => ['store', 'update', 'delete', 'save']]);
    }
    
    public function list(Request $request)
    {
        $appointments = Appointment::with([
            'doctor' => function ($query) use ($request) {
                $query->with('human');
            }, 
            'patient' => function ($query) use ($request) {
                $query->with('user', 'human');
            }, 
        ]);
        if (!!$request->day) {
            $appointments = $appointments->whereDate('start_time', Carbon::parse($request->day));
        }
        return response()->json([
            'appointments' => $appointments->get(),
            'auth' => \AppHelper::refresh($request)
        ], 200);
    }

    public function store(Request $request)
    {
        $appointment = new Appointment();
        $appointment->doctor_id = $request->doctor_id;
        $appointment->patient_id = $request->patient_id;
        $appointment->start_time = $request->start_time;
        $appointment->end_time = $request->end_time;
        $appointment->save();

        return response()->json([
            'appointment' => 'stored',
        ], 200);
    }
}

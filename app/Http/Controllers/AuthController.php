<?php

namespace App\Http\Controllers;

use Tymon\JWTAuth\Exceptions\JWTException;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use App\Http\Controllers\Controller;
use App\Http\Controllers\UserController;
use App\Http\Controllers\PatientController;
use App\Http\Controllers\HumanController;

use App\Http\Requests\RegisterFormRequest;
use App\User;

use JWTAuth;


class AuthController extends Controller
{
    /**
     * Create a new AuthController instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('jwt.auth:api', ['except' => ['login', 'register', 'refresh', 'logout', 'mobileLogin']]);

        $this->middleware('auth.refresh:api', ['except' => ['login', 'register', 'me', 'mobileMe', 'mobileLogin']]);
    }

    /**
     * Register a new user
     *
     * @param  App\ Http\Requests\RegisterFormRequest  $request
     *
     */
    public function register(RegisterFormRequest $request)
    {
        //  Create new User instance
        $user = new User();
        //  Populate user details
        $user->cnp = $request->cnp;
        $user->email = $request->email;
        $user->password = bcrypt($request->password);
        $user->avatar = $request->avatar ? $request->avatar : 'user.png';
        //  Get role if it exists
        $user->role_id = UserController::update_role($user);
        if (!$user->hasRecord()) {
            //  If user does not have record => user does not have a role
            //  Store Human
            $humanStore = HumanController::store($request);
            if ($humanStore['status'] == 'failed') {
                return response()->json($humanStore);
            }
            //  Store Patient
            $patientStore = PatientController::store($request);
            if ($patientStore['status'] == 'failed') {
                return response()->json($patientStore);
            }
            $user->role_id = 4;
        } else if ($user->hasRecord()) {
            //  If user has record => user could have a role
            //  Update Human
            $humanUpdate = HumanController::update($request, $user->human->id);
            if ($humanUpdate['status'] == 'failed') {
                return response()->json($humanUpdate);
            }
            if ($user->role_id > 1) {
                //  Get Role Controller Instance
                $roleModel = 'App\\Http\\Controllers\\' . ucfirst($user->role->name) . 'Controller';
                //  Role Update
                if ($user->role_id > 1) {
                    $roleModelUpdate = $roleModel::update($request, $user->entity()->id);
                    if ($roleModelUpdate['status'] == 'failed') {
                        return response()->json($roleModelUpdate);
                    }
                }
            } else {
                //  Store Patient
                $patientStore = PatientController::store($request);
                if ($patientStore['status'] == 'failed') {
                    return response()->json($patientStore);
                }
                $user->role_id = 4;
            }
        }

        $user->save();

        $credentials = $request->only('email', 'password');
        $token = auth()->attempt($credentials);

        return \AppHelper::respondWithToken($token, 'Bearer', 'success');
    }

    public function login(Request $request)
    {
        $credentials = $request->only('email', 'password');

        try {
            if(! $token = auth()->attempt($credentials)) {
                return \AppHelper::respondWithMessage(
                    'failed',
                    'Invalid credentials',
                    401
                );
            }
        } catch (JWTException $e) {
            return \AppHelper::respondWithMessage(
                'failed',
                $e->getMessage(),
                $e->getStatusCode()
            );
        }

        return \AppHelper::respondWithToken($token, 'Bearer', 'success');
    }

    public function me()
    {
        return response()->json([
            'user' => auth()->user()
        ]);
    }

    public function updateNotification(Request $request)
    {
        auth()->user()->last_notification_read = $request->created_at;
        auth()->user()->save();

        return response()->json([
            'auth' => \AppHelper::refresh($request),
        ], 200);
    }

    public function mobileMe(Request $request)
    {
        return response()->json([
            'id' => auth()->user()->id,
            'email' => auth()->user()->email,
            'auth_token' => $request->headers->get('Authorization'),
        ]);
    }

    public function logout(Request $request)
    {
        auth()->logout();

        return \AppHelper::respondWithMessage(
            'success',
            'Logged out',
            200
        );
    }

    public function refresh(Request $request)
    {
        return response()->json(\AppHelper::refresh($request));
    }

    public function mobileLogin(Request $request)
    {
        $credentials = $request->only('email', 'password');

        try {
            if(! $token = auth()->attempt($credentials)) {
                return response()->json([
                    'status' => 'faild',
                    'message' => 'Invalid credentials',
                ], 401);
            }
        } catch (JWTException $e) {
            return response()->json([
                'status' => 'faild',
                'message' => $e->getMessage(),
            ], $e->getStatusCode());
        }

        return response()->json([
            'id' => auth()->user()->id,
            'auth_token' => $token,
        ], 200);
    }

}

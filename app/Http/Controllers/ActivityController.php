<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Activity;

class ActivityController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth.refresh', ['except' => []]);

        $this->middleware('isAdmin:api', ['except' => []]);

        $this->middleware('isAdminOrSelf:api', ['except' => ['index']]);
    }

    public function index(Request $request)
    {
        $activities = [];
        $colors = [
            'created' => 'success',
            'updated' => 'info',
            'deleted' => 'error',
            'restored' => 'success'
        ];

        $activitiesObject = Activity::orderBy('id', 'desc')->get();

        foreach ($activitiesObject as $activity) {
            if ($activity->subject) {
                $subject = $activity->subject->human;
            } else {
                $subject = (object) [
                    'id' => -1,
                    'first_name' => 'SYSTEM',
                    'last_name' => ''
                ];
            }
            if ($activity->causer) {
                $causer = $activity->causer->human;
            } else {
                $causer = (object) [
                    'id' => -1,
                    'first_name' => 'SYSTEM',
                    'last_name' => ''
                ];
            }

            array_push($activities, [
                'Time Passed' => $activity->created_at->diffForHumans(),
                'On' => $activity->log_name,
                'Action' => [
                    'name' => $activity->description,
                    'color' => $colors[$activity->description]
                ],
                // 'Subject' => [
                //     'name' => $subject->first_name . ' ' . $subject->last_name,
                //     'id' => $subject->id,
                //     'on_click' => explode('\\', $activity->subject_type)[1],
                //     'color' => 'warning'
                // ],
                // 'Causer' => [
                //     'name' => $causer->first_name . ' ' . $causer->last_name,
                //     'id' => $causer->id,
                //     'on_click' => explode('\\', $activity->causer_type)[1],
                //     'color' => 'info'
                // ],
            ]);
        }

        return response()->json([
            'activities' => $activities,
            'auth' => \AppHelper::refresh($request)
        ], 200);
    }
}

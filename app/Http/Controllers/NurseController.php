<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class NurseController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth.refresh', ['except' => []]);

        $this->middleware('isAdmin:api', ['except' => ['show']]);

        $this->middleware('isAdminOrSelf:api', ['except' => []]);
    }

    public static function store($request)
    {
        //  Check if nurse need to be restored
        $nurseTrashedId = \AppHelper::shouldRestoreEntity($request, 'Nurse');
        //  Create new instance 
        if ($nurseTrashedId > 0) {
            $nurse = Nurse::find($nurseTrashedId);
        } else {
            $nurse = new Nurse();
        }
        //  Save Nurse
        return NurseController::save($request, $nurse);
    }

    public static function update(Request $request, $id) 
    {
        $nurse = Nurse::withTrashed()->find($id);

        if ($nurse->trashed()) {
            $nurse->restore();
        }

        return NurseController::save($request, $nurse);
    }

    public static function save(Request $request, Nurse $nurse)
    {
        $nurse->cnp = $request->cnp;

        if (!$nurse->save()) {
            return [
                'status' => 'failed',
                'message' => 'Nurse could not be stored.',
                'auth' => \AppHelper::refresh($request)
            ];
        }

        return [
            'status' => 'success',
            'message' => 'Nurse stored.',
            'auth' => \AppHelper::refresh($request)
        ];
    }
}

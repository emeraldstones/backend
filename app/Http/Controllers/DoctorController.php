<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Controllers\HumanController;

use App\Doctor;
use JWTAuth;
//  TODO: Make this controller as PatientController
class DoctorController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth.refresh', ['except' => []]);

        $this->middleware('isAdmin:api', ['except' => ['show']]);

        $this->middleware('isAdminOrSelf:api', ['except' => ['store', 'update', 'delete', 'save']]);
    }

    public static function store($request)
    {
        //  Check if doctor need to be restored
        $doctorTrashedId = \AppHelper::shouldRestoreEntity($request, 'Doctor');
        //  Create new instance 
        if ($doctorTrashedId > 0) {
            $doctor = Doctor::find($doctorTrashedId);
        } else {
            $doctor = new Doctor();
        }
        //  Save Doctor
        return DoctorController::save($request, $doctor);
    }

    public static function update(Request $request, $id) 
    {
        $doctor = Doctor::withTrashed()->find($id);

        if ($doctor->trashed()) {
            $doctor->restore();
        }

        return DoctorController::save($request, $doctor);
    }

    public static function save(Request $request, Doctor $doctor)
    {
        $doctor->cnp = $request->cnp;

        if (!$doctor->save()) {
            return [
                'status' => 'failed',
                'message' => 'Doctor could not be stored.',
                'auth' => \AppHelper::refresh($request)
            ];
        }

        return [
            'status' => 'success',
            'message' => 'Doctor stored.',
            'auth' => \AppHelper::refresh($request)
        ];
    }

    public function index(Request $request)
    {
        $doctors = Doctor::with([
            'human',
        ])->get();

        return response()->json([
            'auth' => \AppHelper::refresh($request),
            'count' => count($doctors),
            'results' => $doctors,
        ], 200);
    }

    public function list(Request $request)
    {
        $doctors = Doctor::with('human', 'schedule', 'avgRating')
            ->whereHas('schedule', function ($query) use ($request) {
                if ($request->day) {
                    $query->where('day_of_week', $request->day);
                }
            })
            ->get();
        return response()->json([
            'auth' => \AppHelper::refresh($request),
            'count' => count($doctors),
            'doctors' => $doctors,
        ], 200);
    }
}

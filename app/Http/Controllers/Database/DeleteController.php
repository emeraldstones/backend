<?php

namespace App\Http\Controllers\Database;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Patient;
use App\Doctor;
use App\Nurse;

class DeleteController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth.refresh', ['except' => []]);

        $this->middleware('isAdmin:api', ['except' => []]);
    }
    
    //  Delete Patient by ID
    public function deletePatient(Request $request, $id)
    {
        return response()->json(\AppHelper::deleteEntity($request, $id, 'Patient'));
    }
    //  Delete Doctor by ID
    public function deleteDoctor(Request $request, $id)
    {
        return response()->json(\AppHelper::deleteEntity($request, $id, 'Doctor'));
    }
    //  Delete Nurse by ID
    public function deleteNurse(Request $request, $id)
    {
        return response()->json(\AppHelper::deleteEntity($request, $id, 'Nurse'));
    }
}

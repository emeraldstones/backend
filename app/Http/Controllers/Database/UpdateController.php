<?php

namespace App\Http\Controllers\Database;

use Illuminate\Http\Request;
use App\Http\Requests\UserUpdateRequest;
use App\Http\Requests\HumanFormRequest;
use App\Http\Requests\PatientFormRequest;
use App\Http\Requests\DoctorFormRequest;
use App\Http\Requests\NurseFormRequest;

use App\Http\Controllers\Controller;
use App\Http\Controllers\UserController;
use App\Http\Controllers\HumanController;
use App\Http\Controllers\PatientController;
use App\Http\Controllers\DoctorController;
use App\Http\Controllers\NurseController;

class UpdateController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth.refresh', ['except' => []]);

        $this->middleware('isAdminOrSelf:api', ['except' => []]);
    }

    public function updateUser(UserUpdateRequest $request, $id)
    {
        return response()->json(UserController::update($request, $id));
    }

    public function updatePatient(PatientFormRequest $request, $id)
    {
        $patient = \App\Patient::withTrashed()->with('human')->find($id);
        if (!empty($patient->user)) {
            UserController::update($request, $patient->user->id);
        } else {
            //  Human update
            $humanUpdate = HumanController::update($request, $patient->human->id);
            if ($humanUpdate['status'] == 'failed') {
                return response()->json($humanUpdate);
            }
            //  Patient Update
            $patientUpdate = PatientController::update($request, $id);
            if ($patientUpdate['status'] == 'failed') {
                return response()->json($patientUpdate);
            }
        }

        return response()->json([
            'status' => 'success',
            'message' => 'Patient updated.',
            'auth' => \AppHelper::refresh($request)
        ]);
    }

    public function updateDoctor(Request $request, $id)
    {
        $doctor = \App\Doctor::withTrashed()->find($id);
        //  Human update
        $humanUpdate = HumanController::update($request, $doctor->human->id);
        if ($humanUpdate['status'] == 'failed') {
            return response()->json($humanUpdate);
        }
        //  Doctor Update
        $doctorUpdate = DoctorController::update($request, $id);
        if ($doctorUpdate['status'] == 'failed') {
            return response()->json($doctorUpdate);
        }

        return response()->json([
            'status' => 'success',
            'message' => 'Doctor updated.',
            'auth' => \AppHelper::refresh($request)
        ]);
    }

    public function updateNurse(Request $request, $id)
    {
        $nurse = App\Nurse::withTrashed()->find($id);
        //  Human update
        $humanUpdate = HumanController::update($request, $nurse->human->id);
        if ($humanUpdate['status'] == 'failed') {
            return response()->json($humanUpdate);
        }
        //  Nurse Update
        $nurseUpdate = NurseController::update($request, $id);
        if ($nurseUpdate['status'] == 'failed') {
            return response()->json($nurseUpdate);
        }

        return response()->json([
            'status' => 'success',
            'message' => 'Nurse updated.',
            'auth' => \AppHelper::refresh($request)
        ]);
    }
}

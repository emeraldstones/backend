<?php

namespace App\Http\Controllers\Database;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;
use App\Http\Controllers\HumanController;
use App\Http\Controllers\PatientController;
use App\Http\Controllers\DoctorController;
use App\Http\Controllers\NurseController;

use App\Http\Requests\HumanFormRequest;
use App\Http\Requests\PatientFormRequest;
use App\Http\Requests\DoctorFormRequest;
use App\Http\Requests\NurseFormRequest;

class StoreController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth.refresh', ['except' => []]);

        $this->middleware('isAdmin:api', ['except' => []]);
    }

    public function storePatient(PatientFormRequest $request)
    {
        //  Store Human
        $humanStore = HumanController::store($request);
        if ($humanStore['status'] == 'failed') {
            return response()->json($humanStore);
        }
        //  Store Patient
        return response()->json(PatientController::store($request), 200);
    }

    public function storeDoctor(DoctorFormRequest $request)
    {
        //  Store Human
        $humanStore = HumanController::store($request);
        if ($humanStore['status'] == 'failed') {
            return response()->json($humanStore);
        }
        //  Store Doctor
        return response()->json(DoctorController::store($request), 200);
    }

    public function storeNurse(NurseFormRequest $request)
    {
        //  Store Human
        $humanStore = HumanController::store($request);
        if ($humanStore['status'] == 'failed') {
            return response()->json($humanStore);
        }
        //  Store Nurse
        return response()->json(NurseController::store($request), 200);
    }
}

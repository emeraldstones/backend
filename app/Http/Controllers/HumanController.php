<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Schema;

use App\Http\Requests\RegisterFormRequest;

use App\Human;

class HumanController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth.refresh', ['except' => ['register']]);

        $this->middleware('isAdmin:api', ['except' => ['show', 'register']]);

        $this->middleware('isAdminOrSelf:api', ['except' => ['register']]);
    }

    public static function store($request)
    {
        $humanRestoreId = HumanController::shouldRestore($request);

        $cnp = HumanController::cnpChecker($request);

        if ($cnp['status'] == 'failed') {
            return $cnp;
        }
        
        if ($humanRestoreId == -1) {
            $human = new Human();
        } else {
            $human = Human::find($humanRestoreId);
        }

        return HumanController::save($request, $human);
    }

    public static function update($request, $id)
    {
        $cnp = HumanController::cnpChecker($request);

        // if ($cnp['status'] == 'failed') {
        //     return $cnp;
        // }

        $human = Human::withTrashed()->find($id);

        if ($human->trashed()) {
            $human->restore();
        }
        return HumanController::save($request, $human);
    }

    public static function save($request, Human $human)
    {
        foreach($request->all() as $requestProperty => $requestValue) {
            if ($requestProperty != 'id' && Schema::hasColumn($human->getTable(), $requestProperty)) {
                $human[$requestProperty] = $requestValue;
            }
        }

        if (!$human->save()) {
            return [
                'status' => 'failed',
                'message' => 'Human could not be stored.',
                'auth' => \AppHelper::refresh($request)
            ];
        }
        
        return [
            'status' => 'success',
            'message' => 'Human stored.'
        ];
    }

    public static function cnpChecker($request)
    {
        if ($request->cnp && $request->birth_date) {
            $cnp = [
                'sexDigit' => $request->cnp[0],
                'yearDigits' => $request->cnp[1] . $request->cnp[2],
                'monthDigits' => $request->cnp[3] . $request->cnp[4],
                'dayDigits' => $request->cnp[5] . $request->cnp[6]
            ];
            
            $dateTime = explode(' ', $request->birth_date);
            $date = explode('-', $dateTime[0]);
            $dateFirstDigits = $date[0][0] . $date[0][1];
            $dateLastDigits = $date[0][2] . $date[0][3];

            if (
                $cnp['sexDigit'] % 2 == 0 && ucfirst($request->gender) != 'Female'          ||
                $cnp['sexDigit'] % 2 != 0 && ucfirst($request->gender) != 'Male'            ||
                $cnp['sexDigit'] > 0 && $cnp['sexDigit'] < 3 && $dateFirstDigits != '19'    ||
                $cnp['sexDigit'] > 2 && $cnp['sexDigit'] < 5 && $dateFirstDigits != '18'    ||
                $cnp['sexDigit'] > 4 && $cnp['sexDigit'] < 7 && $dateFirstDigits != '20'    ||
                $cnp['yearDigits'] != $dateLastDigits                                       ||
                $cnp['monthDigits'] != $date[1]                                             ||
                $cnp['dayDigits'] != $date[2]
            ) {
                return [
                    'status' => 'failed',
                    'message' => 'CNP does not correspond with gender or birth date',
                    'auth' => \AppHelper::refresh($request),
                ];
            }
        } else {
            return [
                'status' => 'no-values',
            ];
        }

        return [
            'status' => 'success'
        ];
    }

    public static function shouldRestore($request)
    {
        $trashedHumans = Human::onlyTrashed()->get();

        foreach ($trashedHumans as $human) {
            if ($request->cnp == $human->cnp) {
                $human->restore();
                return $human->id;
            }
        }

        return -1;
    }
}

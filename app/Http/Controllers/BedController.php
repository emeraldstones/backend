<?php

namespace App\Http\Controllers;

use App\Bed;
use App\Patient;
use Illuminate\Http\Request;

class BedController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth.refresh', ['except' => []]);

        $this->middleware('isAdmin:api', ['except' => ['show']]);

        $this->middleware('isAdminOrSelf:api', ['except' => ['store', 'update', 'delete', 'save']]);
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Bed  $bed
     * @return \Illuminate\Http\Response
     */
    public function show(Bed $bed)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Bed  $bed
     * @return \Illuminate\Http\Response
     */
    public function edit(Bed $bed)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Bed  $bed
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Bed $bed)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Bed  $bed
     * @return \Illuminate\Http\Response
     */
    public function destroy(Bed $bed)
    {
        //
    }

    public function availableBeds(Request $request)
    {
        $busyBeds = [];
        $availableBeds = [];
        $beds = Bed::with('room')->get();
        $patients = Patient::with([
            'bed' => function($query) use ($request) {
                $query->with('room');
            },
        ])->get();
        
        foreach($patients as $patient) {
            if ($patient->bed) {
                array_push($busyBeds, $patient->bed);
            }
        }

        foreach($beds as $bed) {
            if (!in_array($bed, $busyBeds)) {
                array_push($availableBeds, $bed);
            }
        }

        return response()->json([
            'auth' => \AppHelper::refresh($request),
            'count' => count($availableBeds),
            'results' => $availableBeds,
        ], 200);
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Schema;

use App\Patient;

class PatientController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth.refresh', ['except' => []]);

        $this->middleware('isAdmin:api', ['except' => ['show']]);

        $this->middleware('isAdminOrSelf:api', ['except' => ['store', 'update', 'delete', 'save']]);
    }

    public static function store($request)
    {
        //  Check if patient need to be restored
        $patientTrashedId = \AppHelper::shouldRestoreEntity($request, 'Patient');
        //  Create new instance 
        if ($patientTrashedId > 0) {
            $patient = Patient::find($patientTrashedId);
        } else {
            $patient = new Patient();
        }
        //  Save Patient
        return PatientController::save($request, $patient);
    }

    public static function update(Request $request, $id) 
    {
        $patient = Patient::withTrashed()->find($id);

        if ($patient->trashed()) {
            $patient->restore();
        }

        return PatientController::save($request, $patient);
    }

    public static function save(Request $request, Patient $patient)
    {
        foreach($request->all() as $requestProperty => $requestValue) {
            if ($requestProperty != 'id' && Schema::hasColumn($patient->getTable(), $requestProperty)) {
                if ($requestProperty === 'emergency') {
                    $patient['emergency_at'] = \Carbon\Carbon::now();
                } 
                if ($requestProperty === 'hospitalized') {
                    $patient['hospitalized_at'] = \Carbon\Carbon::now();
                }
                if ($requestProperty === 'monitoring') {
                    $patient['monitoring_at'] = \Carbon\Carbon::now();
                }
                $patient[$requestProperty] = $requestValue;
            }
        }

        if ($request->emergency || $request->hospitalize) {
            $user = \App\User::with('doctor')->find(auth()->user()->id);
            if ($user->doctor) {
                $patient->doctor_id = $user->doctor->id;
            }
        }

        if (!$patient->save()) {
            return [
                'status' => 'failed',
                'message' => 'Patient couldn\'t be stored.',
                'auth' => auth()->user() ? \AppHelper::refresh($request) : 'null'
            ];
        }

        return [
            'status' => 'success',
            'message' => 'Patient stored.',
            'auth' => auth()->user() ? \AppHelper::refresh($request) : 'null'
        ];
    }

    public function index(Request $request)
    {
        $patients = [];
        foreach (Patient::with(['human'])->get() as $patient) {
            array_push($patients, [
                'ID' => $patient->id,
                'First name' => $patient->human->first_name,
                'Last name' => $patient->human->last_name,
                'CNP' => $patient->cnp,
                'BPM' => $patient->bpm,
                'Temperature' => $patient->temperature,
                'Hospitalized' => $patient->hospitalized_at
                // 'Phone NO.' => $patient->human->phone_no,
                // 'Email' => $patient->human->email,
                // 'Gender' => $patient->human->gender,
                // 'Martial Status' => $patient->human->martialStatus,
                // 'Weight' => $patient->human->weight,
                // 'Address' => $patient->human->address, 
                // 'Nationality' => $patient->human->nationality,
            ]);
        }

        return response()->json([
            'patients' => $patients,
            'auth' => \AppHelper::refresh($request)
        ], 200);
    }

    public function list(Request $request)
    {
        $patients = Patient::with([
            'human',
            'bed' => function($query) use ($request) {
                $query->with('room');
            },
            'records' => function($query) use ($request) {
                $query->with([
                    'doctor' => function($query) use ($request) {
                        $query->with('human');
                    }
                ]);
            }
        ]);

        if ($request->monitoring === 'true') {
            $patients = $patients->where('monitoring', 'like', true);
        }

        if ($request->hospitalized  === 'true') {
            $patients = $patients->whereNotNull('hospitalized_at');
        }

        if ($request->emergency  === 'true') {
            $patients = $patients->whereNotNull('emergency_at');
        }

        $patients = $patients->get();
  
        return response()->json([
            'auth' => \AppHelper::refresh($request),
            'count' => count($patients),
            'results' => $patients,
        ], 200);
    }

    public function mobileList(Request $request)
    {
        $patients = new Patient();
        
        if ($request->monitoring === 'true') {
            $patients = $patients->where('monitoring', 'like', true);
        }

        if ($request->hospitalized  === 'true') {
            $patients = $patients->whereNotNull('hospitalized_at');
        }

        if ($request->emergency  === 'true') {
            $patients = $patients->whereNotNull('emergency_at');
        }

        $patients = $patients->join('humans', 'patients.cnp', '=', 'humans.cnp')->select('patients.*', 'humans.first_name', 'humans.last_name')->get();
  
        return response()->json($patients, 200);
    }
}

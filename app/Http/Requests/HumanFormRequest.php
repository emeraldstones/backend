<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Validation\Rule;

use Illuminate\Http\Exceptions\HttpResponseException;

class HumanFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'first_name' => 'required|string|min:6',
            'last_name' => 'required|string|min:3',
            'birth_date' => 'required|date',
            'phone_no' => 'required|numeric|min:5',
            'gender' => [
                'required',
                Rule::in(['male', 'Male', 'female', 'Female'])
            ],
            'martial_status' => [
                'required',
                Rule::in(['married', 'Married', 'single', 'Single'])
            ],
            'address' => 'required|max:300',
            'country' => 'required|max:15',
            'nationality' => 'required|max:15',
            'cnp' => 'required|string|min:13|max:13|unique:humans,cnp,NULL,id,deleted_at,NULL',
        ];
    }

    /**
     *  No redirect on fail
     *
     * @return array
     */
    protected function failedValidation(Validator $validator) {
        throw new HttpResponseException(
            response()->json($validator->errors(), 422)
        );
    }
}

<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Validation\Rule;

use Illuminate\Http\Exceptions\HttpResponseException;

class UserUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'first_name' => 'required|string|min:6',
            'last_name' => 'required|string|min:3',
            'birth_date' => 'required|date',
            'phone_no' => 'required|numeric|min:5',
            'gender' => [
                'required',
                Rule::in(['male', 'Male', 'female', 'Female'])
            ],
            'martial_status' => [
                'required',
                Rule::in(['married', 'Married', 'single', 'Single'])
            ],
            'address' => 'required|max:300',
            'country' => 'required|max:15',
            'nationality' => 'required|max:15',
            'password' => 'string|min:6|max:13',
            'cnp' => 'required|string|min:13|max:13|unique:users,cnp,' . request()->id,
            'email' => 'required|email|unique:users,email,' . request()->id,
        ];
    }

    /**
     * Custom message for validation
     *
     * @return array
     */
    public function messages()
    {
        return [
            'email.required' => 'Email is required!',
            'first_name.required' => 'First name is required!',
            'last_name.required' => 'Last name is required!',
            'cnp.required' => 'CNP is required!',
            'password.required' => 'Password is required!'
        ];
    }

    /**
     *  Filters to be applied to the input.
     *
     * @return array
     */
    public function filters()
    {
        return [
            'email' => 'trim|lowercase',
            'first_name' => 'trim|capitalize|escape',
            'last_name' => 'trim|capitalize|escape',
            'gender' => 'trim|capitalize|escape',
            'martial_status' => 'trim|capitalize|escape'
        ];
    }

    /**
     *  No redirect on fail
     *
     * @return array
     */
    protected function failedValidation(Validator $validator) {
        throw new HttpResponseException(
            response()->json($validator->errors(), 422)
        );
    }
}

<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Validation\Rule;

use Illuminate\Http\Exceptions\HttpResponseException;

class PatientFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if (request()->id) {
            $patient = \App\Patient::find(request()->id);
        } else {
            $patient = null;
        }
        
        return [
            'first_name' => 'string|min:3',
            'last_name' => 'string|min:3',
            'birth_date' => 'date',
            'phone_no' => 'numeric|min:5',
            'gender' => [
                Rule::in(['male', 'Male', 'female', 'Female'])
            ],
            'martial_status' => [
                Rule::in(['married', 'Married', 'single', 'Single'])
            ],
            'address' => 'max:300',
            'country' => 'max:30',
            'nationality' => 'max:30',
            'cnp' => !$patient ? 
                'string|min:13|max:13|unique:patients,cnp,NULL,id,deleted_at,NULL' :
                'string|min:13|max:13|unique:patients,cnp,' . $patient->id . ',id,deleted_at,NULL',
        ];
    }

    /**
     *  No redirect on fail
     *
     * @return array
     */
    protected function failedValidation(Validator $validator) {
        throw new HttpResponseException(
            response()->json($validator->errors(), 422)
        );
    }
}

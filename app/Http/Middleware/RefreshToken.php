<?php

namespace App\Http\Middleware;

use Illuminate\Support\Facades\Auth;
use Closure;

class RefreshToken
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        try {
            \JWTAuth::parseToken()->authenticate();
        } catch (\Tymon\JWTAuth\Exceptions\TokenExpiredException $e) {
            try {
                $refreshed_token = \JWTAuth::refresh(\JWTAuth::getToken());
                $request->headers->set('Authorization', 'Bearer ' . $refreshed_token);
                $request->request->add(['status' => 'changed']);
                auth()->login(\JWTAuth::setToken($refreshed_token)->toUser(), true);

                return $next($request);
            } catch (\Tymon\JWTAuth\Exceptions\TokenExpiredException $e) {
                $request->request->add(['status' => 'failed']);
                $request->request->add(['message' => 'Token expired']);
                
                return response([
                    'status' => 'failed',
                    'message' => 'Token expired'
                ], 401);
            }
        }

        $request->request->add(['status' => 'no-change']);
        return $next($request);
    }
}

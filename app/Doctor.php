<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;
use Illuminate\Database\Eloquent\SoftDeletes;

class Doctor extends Model
{
    use LogsActivity, SoftDeletes;

    protected static $logName = 'doctors';

    protected $hidden = ['created_at', 'updated_at'];

    public function human()
    {
        return $this->belongsTo('App\Human', 'cnp', 'cnp')->withTrashed();
    }

    public function patient()
    {
        return $this->hasMany('App\Patient');
    }

    public function user()
    {
        return $this->hasOne('App\User', 'cnp', 'cnp');
    }

    public function appointment()
    {
        return $this->hasMany('App\Appointment');
    }

    public function schedule()
    {
        return $this->hasMany('App\DoctorSchedule');
    }

    public function rating()
    {
        return $this->hasMany('App\Rating');
    }

    public function avgRating()
    {
        return $this->rating()
            ->selectRaw('avg(stars) as stars, doctor_id')
            ->selectRaw('count(*) as count')
            ->groupBy('doctor_id');
    }

    public function records()
    {
        return $this->hasMany('App\Records');
    }
}

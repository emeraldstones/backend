<?php

namespace App;

use Tymon\JWTAuth\Contracts\JWTSubject;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

use Spatie\Activitylog\Traits\LogsActivity;

use App\Patient;

class User extends Authenticatable implements JWTSubject
{
    use Notifiable, LogsActivity;

    protected static $logName = 'user';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'email', 'role_id', 'avatar'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token', 'cnp', 'email_verified_at', 'created_at', 'updated_at'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /**
     * Get the role record associated with the user.
     */
	public function role()
	{
		return $this->belongsTo('App\Role');
	}

	public function human()
	{
		return $this->belongsTo('App\Human', 'cnp', 'cnp')->withTrashed();
	}

	public function doctor()
	{
		return $this->belongsTo('App\Doctor', 'cnp', 'cnp')->withTrashed();
	}

	public function nurse()
	{
		return $this->belongsTo('App\Nurse', 'cnp', 'cnp')->withTrashed();
	}

	public function patient()
	{
		return $this->belongsTo('App\Patient', 'cnp', 'cnp')->withTrashed();
    }

    public function entity()
    {
        if ($this->isDoctor()) {
            return $this->doctor;
        } else if ($this->isNurse()) {
            return $this->nurse;
        } else if ($this->isPatient()) {
            return $this->patient;
        }
    }

	public function hasRecord()
	{
		return !empty($this->human);
	}

	public function isDoctor()
	{
		return !empty($this->doctor);
	}

	public function isNurse()
	{
		return !empty($this->nurse);
	}

	public function isPatient()
	{
		return !empty($this->patient);
	}

    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }
    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }
}

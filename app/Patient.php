<?php

namespace App;

use App\Events\PatientUpdated;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;
use Illuminate\Database\Eloquent\SoftDeletes;

class Patient extends Model
{
    use LogsActivity, SoftDeletes;

    protected static $logName = 'patients';

    protected $dispatchesEvents = [
        'updated' => PatientUpdated::class,
        'created' => PatientUpdated::class,
    ];

    public function human()
    {
        return $this->belongsTo('App\Human', 'cnp', 'cnp')->withTrashed();
    }

    public function doctor()
    {
        return $this->belongsTo('App\Patient');
    }

    public function user()
    {
        return $this->hasOne('App\User', 'cnp', 'cnp');
    }

    public function appointment()
    {
        return $this->hasMany('App\Appointment');
    }

    public function bed()
    {
        return $this->belongsTo('App\Bed');
    }

    public function records()
    {
        return $this->hasMany('App\Records');
    }
}

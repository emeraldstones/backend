<?php

namespace App;

use App\Events\HumanUpdated;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Human extends Model
{
    use SoftDeletes;
    
    protected $hidden = ['id', 'cnp', 'age', 'created_at', 'updated_at'];

    protected $dispatchesEvents = [
        // 'updated' => HumanUpdated::class,
        // 'created' => PatientUpdated::class,
    ];

    public function user()
    {
        return $this->hasOne('App\User', 'cnp', 'cnp');
    }

    public function doctor()
    {
        return $this->hasOne('App\Doctor', 'cnp', 'cnp')->withTrashed();
    }

    public function nurse()
    {
        return $this->hasOne('App\Nurse', 'cnp', 'cnp')->withTrashed();
    }

    public function patient()
    {
        return $this->hasOne('App\Patient', 'cnp', 'cnp')->withTrashed();
    }

    public function hasAccount()
    {
        return !empty($this->user()->find(1));
    }

    public function isDoctor()
    {
        return !empty($this->doctor()->find(1));
    }

    public function isNurse()
    {
        return !empty($this->nurse()->find(1));
    }

    public function isPatient()
    {
        return !empty($this->patient()->find(1));
    }
}

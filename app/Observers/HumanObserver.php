<?php

namespace App\Observers;

use App\Human;

class HumanObserver
{
    /**
     * Handle the human "created" event.
     *
     * @param  \App\Human  $human
     * @return void
     */
    public function created(Human $human)
    {
        //
    }

    /**
     * Handle the human "updated" event.
     *
     * @param  \App\Human  $human
     * @return void
     */
    public function updated(Human $human)
    {
        //
    }

    /**
     * Handle the human "deleted" event.
     *
     * @param  \App\Human  $human
     * @return void
     */
    public function deleted(Human $human)
    {
        //
    }

    /**
     * Handle the human "restored" event.
     *
     * @param  \App\Human  $human
     * @return void
     */
    public function restored(Human $human)
    {
        //
    }

    /**
     * Handle the human "force deleted" event.
     *
     * @param  \App\Human  $human
     * @return void
     */
    public function forceDeleted(Human $human)
    {
        //
    }
}

<?php

namespace App\Observers;

use App\Notification;

class NotificationObserver
{
    public function created(Notification $notification)
    {
        //
    }

    public function updated(Notification $notification)
    {
        //
    }

    public function deleted(Notification $notification)
    {
        //
    }

    public function restored(Notification $notification)
    {
        //
    }

    public function forceDeleted(Notification $notification)
    {
        //
    }
}

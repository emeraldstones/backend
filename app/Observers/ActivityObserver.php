<?php

namespace App\Observers;

use App\Activity;

class ActivityObserver
{
    public function created(Activity $activity)
    {
        //
    }

    public function updated(Activity $activity)
    {
        //
    }

    public function deleted(Activity $activity)
    {
        //
    }

    public function restored(Activity $activity)
    {
        //
    }

    public function forceDeleted(Activity $activity)
    {
        //
    }
}

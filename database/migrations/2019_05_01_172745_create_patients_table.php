<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePatientsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('patients', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('doctor_id')->nullable();
            $table->string('cnp')->unique();
            $table->integer('bpm')->default(-1);
            $table->float('temperature')->default(-1);
            $table->string('blood_presure')->default('120/80');
            $table->string('extern_factors')->default('H2O(0.001 g),');
            $table->string('infection')->default('');

            $table->integer('bed_id')->unique()->nullable();
            $table->integer('severity')->default(5);
            $table->string('departament')->nullable();

            $table->boolean('monitoring')->default(false);
            $table->boolean('hospitalized')->default(false);
            $table->boolean('emergency')->default(false);
            $table->datetime('monitoring_at')->nullable();
            $table->datetime('emergency_at')->nullable();
            $table->datetime('hospitalized_at')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('patients');
    }
}

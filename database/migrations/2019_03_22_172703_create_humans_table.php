<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHumansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('humans', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('cnp')->unique();
            $table->string('first_name');
            $table->string('last_name');
            $table->dateTime('birth_date');
            $table->string('phone_no');
            $table->string('email');
            $table->string('gender');
            $table->string('martial_status');
            $table->integer('weight')->default('0')->nullable();
            $table->string('address')->default('')->nullable();
            $table->string('country')->default('')->nullable();
            $table->string('nationality')->default('')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('humans');
    }
}

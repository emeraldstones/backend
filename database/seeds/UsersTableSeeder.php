<?php

use Illuminate\Support\Str;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

use Illuminate\Support\Facades\Auth;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $admins = [
            [
                'name' => ['Razvan', 'Miu'],
                'email' => 'razvan.miu@yahoo.com',
                'password' => bcrypt('secret'),
                'role_id' => 1,
                'cnp' => '1970428033343',
                'birth_date' => '1997-04-28 00:00:00',
                'phone_no' => '40742633605',
                'gender' => 'Male',
                'martial_status' => 'Single',
                'weight' => '71',
                'address' => 'Strada Elena Cuza, Bl. B4, Sc. B, Ap. 26',
                'country' => 'Romania',
                'nationality' => 'Romanian'
            ],
        ];

        $dummyUsers = [
            [
                'name' => ['Bogdan', 'Miu'],
                'email' => 'bogdan.miu@yahoo.com',
                'password' => bcrypt('secret'),
                'role_id' => 4,
                'cnp' => '1921019033343',
                'birth_date' => '1992-10-19 00:00:00',
                'phone_no' => '40742633605',
                'gender' => 'Male',
                'martial_status' => 'Single',
                'weight' => '71',
                'address' => 'Strada Elena Cuza, Bl. B4, Sc. B, Ap. 26',
                'country' => 'Romania',
                'nationality' => 'Romanian'
            ],
        ];

        foreach ($admins as $admin) {
            DB::table('users')->insert([
                'email' => $admin['email'],
                'password' => $admin['password'],
                'role_id' => $admin['role_id'],
                'cnp' => $admin['cnp'],
            ]);

            $user = \App\User::find(1);

            auth()->login($user);

            DB::table('humans')->insert([
                'cnp' => $admin['cnp'],
                'first_name' => $admin['name'][0],
                'last_name' => $admin['name'][1],
                'birth_date' => $admin['birth_date'],
                'phone_no' => $admin['phone_no'],
                'email' => $admin['email'],
                'gender' => $admin['gender'],
                'martial_status' => $admin['martial_status'],
                'weight' => $admin['weight'],
                'address' => $admin['address'],
                'country' => $admin['country'],
                'nationality' => $admin['nationality'],
            ]);

            DB::table('doctors')->insert([
                'cnp' => $admin['cnp'],
                'speciality' => 'Admin',
            ]);
        }

        foreach ($dummyUsers as $dummy) {
            DB::table('users')->insert([
                'email' => $dummy['email'],
                'password' => $dummy['password'],
                'role_id' => $dummy['role_id'],
                'cnp' => $dummy['cnp'],
            ]);

            DB::table('humans')->insert([
                'cnp' => $dummy['cnp'],
                'first_name' => $dummy['name'][0],
                'last_name' => $dummy['name'][1],
                'birth_date' => $dummy['birth_date'],
                'phone_no' => $dummy['phone_no'],
                'email' => $dummy['email'],
                'gender' => $dummy['gender'],
                'martial_status' => $dummy['martial_status'],
                'weight' => $dummy['weight'],
                'address' => $dummy['address'],
                'country' => $dummy['country'],
                'nationality' => $dummy['nationality'],
            ]);

            DB::table('patients')->insert([
                'cnp' => $dummy['cnp'],
                'bpm' => '-1',
                'temperature' => '37',
                'hospitalized_at' => Carbon\Carbon::now()
            ]);
        }
    }
}

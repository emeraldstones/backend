<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $roles = [
            'admin' => '2',
            'doctor' => '1',
            'nurse' => '1',
            'patient' => '0'
        ];

        foreach ($roles as $role => $access_level) {
            DB::table('roles')->insert([
                'name' => $role,
                'access_level' => $access_level,
                'created_at' => now(),
                'updated_at' => now()
            ]);
        }
    }
}

<?php

use Illuminate\Database\Seeder;

use App\Room;

class RoomsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $rooms = ['General', 'Pediatry', 'Cardiology'];
        foreach($rooms as $key => $room) {
            for($i = 0; $i < 30; $i++) {
                $newRoom = new Room();
                $newRoom->type = $room;
                $newRoom->no = $key;
                $no = $i > 9 ? $i : '0' . $i;
                $newRoom->no =  $newRoom->no . $no;
                $newRoom->beds = rand(5, 30);
                $newRoom->save();
            }
        }
    }
}
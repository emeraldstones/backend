<?php

use Illuminate\Database\Seeder;

use App\Room;
use App\Bed;

class BedSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $rooms = Room::all();
        foreach($rooms as $key => $room) {
            for($i = 0; $i < $room->beds; $i++) {
                $newBed = new Bed();
                $newBed->room_id = $room->id;
                $newBed->no = $i;
                $newBed->save();
            }
        }
    }
}
<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            UsersTableSeeder::class,
            RolesTableSeeder::class,
            HumansTableSeeder::class,
            DoctorsTableSeeder::class,
            PatientsTableSeeder::class,
            DoctorScheduleSeeder::class,
            RatingSeeder::class,
            RoomsSeeder::class,
            BedSeeder::class,
        ]);
    }
}

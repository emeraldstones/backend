<?php

use Illuminate\Database\Seeder;

use App\Doctor;
use App\DoctorSchedule;

class DoctorScheduleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $doctors = Doctor::all();
        $daysOfWeek = ['MO', 'TU', 'WE', 'TH', 'FR', 'SA', 'SU'];
        $startHours = ['06:00', '12:00', '18:00'];
        $endHours = ['12:00', '18:00', '02:00'];
        foreach($doctors as $doctor) {
            foreach($daysOfWeek as $day) {
                $rand = rand(0, 2);
                $schedule = new DoctorSchedule();
                $schedule->doctor_id = $doctor->id;
                $schedule->day_of_week = $day;
                $schedule->start_time = $startHours[$rand];
                $schedule->end_time = $endHours[$rand];
                $schedule->save();
            }
        }
    }
}
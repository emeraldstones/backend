<?php

use Illuminate\Database\Seeder;

use App\Doctor;
use App\Rating;

class RatingSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $doctors = Doctor::all();
        foreach($doctors as $doctor) {
            $ratingsNo = rand(0, 10);
            while($ratingsNo > 0) {
                $rating = new Rating();
                $rating->doctor_id = $doctor->id;
                $rating->user_id = 1;
                $rating->stars = rand(0, 5);
                $rating->save();
                $ratingsNo--;
            }
        }
    }
}
<?php

use Faker\Generator as Faker;

$factory->define(App\Patient::class, function (Faker $faker) {
    $hospitalized_at = rand(0, 1) == 1 ? Carbon\Carbon::now() : NULL;
    // $monitoring_at = $hospitalized_at ? NULL : Carbon\Carbon::now();
    $monitoring_at = NULL;
    $hospitalized = $hospitalized_at ? true : false;
    $monitoring = $monitoring_at ? true: false;

    if ($hospitalized) {
        $alergies = ['', 'Egg', 'Fish', 'Wheat', 'Cat', 'Paraphenylenediamine'];
        $diagnostics = [
            "Lorem Ipsum is simply dummy text of the printing.",
            "Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC.",
        ];
        $medication = [
            "Atenolol, Aspirin",
            "hydroxyzine HCl",
            "Bactrim DS"
        ];     
        $medical_record = new App\Records();
        $medical_record->patient_id = 2;
        $medical_record->doctor_id = 2;
        $medical_record->known_alergies = $alergies[rand(0, count($alergies) - 1)];
        $medical_record->diagnostic = $diagnostics[rand(0, count($diagnostics) - 1)];
        $medical_record->medication = $medication[rand(0, count($medication) - 1)];
        $medical_record->save();
    }

    $departaments = ['Cardiology', 'Burn center', 'Gynecology', 'Haematology', 'Maternity'];

    return [
        'cnp' => factory(App\Human::Class)->create()->cnp,
        'bpm' => rand(50, 100),
        'temperature' => rand(36, 37),
        'departament' => $departaments[rand(0, count($departaments) - 1)],
        'monitoring' => $monitoring,
        'hospitalized' => $hospitalized,
        'emergency' => false,
        'monitoring_at' => $hospitalized_at,
        'hospitalized_at' => $hospitalized_at,
        'emergency_at' => NULL,
    ];
});

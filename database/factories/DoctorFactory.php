<?php

use Faker\Generator as Faker;

$factory->define(App\Doctor::class, function (Faker $faker) {
    $specialities = ['Cardiolog', 'Dentist'];
    return [
        'cnp' => factory(App\Human::Class)->create()->cnp,
        'speciality' => $specialities[rand(0, count($specialities) - 1)]
    ];
});

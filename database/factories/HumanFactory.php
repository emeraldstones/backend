<?php

use Faker\Generator as Faker;

$factory->define(App\Human::class, function (Faker $faker) {
    $faker->addProvider(new \Faker\Provider\ro_RO\Person($faker));
    $faker->addProvider(new \Faker\Provider\ro_RO\PhoneNumber($faker));

    $birthDate = $faker->date('Y-m-d', $max = '1992-01-01');
    $gender = $faker->randomElement([
        \Faker\Provider\ro_RO\Person::GENDER_MALE,
        \Faker\Provider\ro_RO\Person::GENDER_FEMALE
    ]);
    $country = $faker->country;

    return [
        'cnp' => $faker->cnp($gender, $birthDate),
        'first_name' => $faker->firstName($gender),
        'last_name' => $faker->lastName,
        'birth_date' => $birthDate,
        'phone_no' => $faker->premiumRatePhoneNumber,
        'email' => $faker->email,
        'gender' => ucfirst($gender),
        'martial_status' => $faker->randomElement(['Married', 'Single']),
        'weight' => $faker->numberBetween($min = 60, $max = 90),
        'address' => $faker->address,
        'country' => $country,
        'nationality' => 'RO',
    ];
});
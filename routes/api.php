<?php

use Illuminate\Http\Request;

use App\Events\ActionEvent;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::group([
    'middleware' => 'api',
    'prefix' => 'auth'
], function($router) {

    Route::post('register', 'AuthController@register');
    Route::post('login', 'AuthController@login');
    Route::post('mobile/login', 'AuthController@mobileLogin');
    Route::post('logout', 'AuthController@logout');
    Route::post('refresh', 'AuthController@refresh');
    Route::get('me', 'AuthController@me');
    Route::get('mobile/me', 'AuthController@mobileMe');

    Route::put('update', 'AuthController@updateNotification');
});

Route::group([
    'middleware' => 'api',
    'prefix' => 'chat'
], function($router) {

    Route::get('contacts', 'ContactsController@index');
});

Route::group([
    'middleware' => 'api',
    'prefix' => 'store'
], function($router) {

    Route::post('appointment', 'AppointmentController@store');
    Route::post('patient', 'Database\StoreController@storePatient');
    Route::post('doctor', 'Database\StoreController@storeDoctor');
    Route::post('nurse', 'Database\StoreControlle@storeNurse');
    Route::post('notification', 'NotificationController@store');
});

Route::group([
    'middleware' => 'api',
    'prefix' => 'delete'
], function($router) {
    Route::get('user/{id}', 'UserController@delete');
    Route::get('patient/{id}', 'Database\DeleteController@deletePatient');
    Route::get('doctor/{id}', 'Database\DeleteController@deleteDoctor');
    Route::get('nurse/{id}', 'Database\DeleteController@deleteNurse');
});

Route::group([
    'middleware' => 'api',
    'prefix' => 'update'
], function($router) {
    Route::put('user/{id}', 'Database\UpdateController@updateUser');
    Route::put('patient/{id}', 'Database\UpdateController@updatePatient');
    Route::put('doctor/{id}', 'Database\UpdateController@updateDoctor');
    Route::put('nurse/{id}', 'Database\UpdateController@updateNurse');
});

Route::group([
    'middleware' => 'api',
], function($router) {

    Route::get('users', 'UserController@index');
    Route::get('users/{id}', 'UserController@show');

    Route::get('activities', 'ActivityController@index');

    Route::get('doctors', 'DoctorController@index');
    Route::get('patients', 'PatientController@index');

    Route::get('notifications', 'NotificationController@show');


    Route::get('dashboard', 'DashboardController@fetch');

    Route::get('records/{id}', 'RecordsController@fetch');

    Route::get('appointment', 'AppointmentController@list');
    Route::get('doctor', 'DoctorController@list');
    Route::get('patient', 'PatientController@list');
    Route::get('mobile/patient', 'PatientController@mobileList');
    Route::get('bed/available', 'BedController@availableBeds');
});

Route::group([
    'middleware' => 'api',
], function($router) {
    Route::put('patient/{id}', 'Database\UpdateController@updatePatient');
    Route::put('doctor/{id}', 'Database\UpdateController@updateDoctor');
});